
app.controller('dashboardCtrl', function($scope, $state, $http, ipCookie) {

	$scope.channels = [];

	$http.get('http://demo.omnisttechhub.com/api/channels?token=' + ipCookie('session')).then(function(res) {
		if (res.data.error) {
			location.href = "http://demo.omnisttechhub.com/api/google";
		} else {
			$scope.channels = res.data.data.items.map(function(channel) {
				return {
					id: channel.id,
					title: channel.snippet.title
				}
			});
		}

 		console.log($scope.channels);
	});

	$('body').on('click', '.nav   li ', function() {
		$(this).find('ul').stop(true, true).slideToggle();
	})


});