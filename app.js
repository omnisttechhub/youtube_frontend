var app = angular.module('app', ['ui.router', 'ipCookie'])

app.config(function($stateProvider, $locationProvider, $urlRouterProvider) {
    $stateProvider

        .state('dashboard', {
        url: '/dashboard',
        controller: 'dashboardCtrl',
        templateUrl: 'dashboard.html'
    })

    .state('login', {
        url: '/login',
        controller: 'login',
        templateUrl: 'login.html'
    })


    .state('video', {
        url: '/channels/:channelId/videos',
        controller: 'video',
        templateUrl: 'video.html'
    })


    // $urlRouterProvider.when('/admin/', '/admin/login');
    $urlRouterProvider.otherwise('/dashboard')
        // $locationProvider.html5Mode(true);

    $locationProvider.html5Mode(true).hashPrefix('!');

});


app.controller('main_ctrl', function($scope, $rootScope, ipCookie, $state) {
    $scope.getQuery = function(key) {
        return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
    }

    if (ipCookie('session') || $scope.getQuery('accessToken')) {
        ipCookie('session', $scope.getQuery('accessToken'));
    } else {
        location.href = "http://demo.omnisttechhub.com/api/google";
    }
});

app.controller('login', function($scope) {

});

app.controller('video', function($scope, $stateParams, $http, ipCookie) {
    $scope.videos = [];
    $http.get('http://demo.omnisttechhub.com/api/channels/' + $stateParams.channelId + '/videos?token=' + ipCookie('session')).then(function(res) {
        if (res.data.error) {
            location.href = "http://demo.omnisttechhub.com/api/google";
        } else {
            $scope.videos = res.data.data.items.map(function(video) {
                return {
                    id: video.id.videoId,
                    title: video.snippet.title
                }
            }).filter(function(video) {
                return video.id;
            });
        }

        console.log($scope.channels);
    });
});

app.directive('sidebar', function() {
    return {
        restrict: 'EA',
        templateUrl: 'sidebar.html'
    }
});